#include <SFML/Graphics.hpp>
#include <vector>
#include <sstream>
#include <iostream>

#include "graph.h"
#include "physics.h"

void error(std::string str)
{
    std::cerr << "ERROR: " << str << '\n';
}

int main()
{
    setlocale(LC_CTYPE, "rus");
    std::string app_name = "Planets by AArtur";
    sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), app_name);
    
    ScrollField log_field(LOG_FIELD_X, LOG_FIELD_Y, LOG_FIELD_DX, LOG_FIELD_DY, LOG_FONT_SIZE, LOG_FONT_GAP);
    ScrollField info_field(INFO_FIELD_X, INFO_FIELD_Y, INFO_FIELD_DX, INFO_FIELD_DY, INFO_FONT_SIZE, INFO_FONT_GAP);
    ScrollField detailed_info_field(DETAILED_INFO_FIELD_X, DETAILED_INFO_FIELD_Y,
        DETAILED_INFO_FIELD_DX, DETAILED_INFO_FIELD_DY, DETAILED_INFO_FONT_SIZE, DETAILED_INFO_FONT_GAP);
    
    std::vector<std::string> log;
    std::vector<std::string> objects_info;
    std::vector<const CelestialBody *> info_object_ptrs;
    
    bool log_scrolling = false, info_scrolling = false, detailed_info_scrolling = false;
    
    TextField comet_input_field(COMET_INPUT_X, COMET_INPUT_Y, COMET_INPUT_DX, COMET_INPUT_DY,
        COMET_INPUT_MAX_SIZE, COMET_INPUT_FONT_SIZE);
    std::string comet_name;
    double comet_weight, comet_speed, comet_radius;
    double comet_x, comet_y;
    double comet_second_x, comet_second_y;
    double comet_speed_x, comet_speed_y;
    
    std::string default_comet_name = "BigComet";
    std::string default_comet_weight = "6.0000e20";
    std::string default_comet_speed = "100000";
    std::string default_comet_radius = "6.0000e5";

    std::string default_filepath = "planet_systems/solar_system.txt";
    std::string filepath = default_filepath;
    TextField filepath_input_field(USER_INPUT_X, USER_INPUT_Y, USER_INPUT_DX, USER_INPUT_DY,
        USER_FILEPATH_FONT_SIZE, FILEPATH_MAX_SIZE);
    filepath_input_field.set_string(filepath);
    
    GraphStorage pictures;
    
    if (!pictures.load_backs(BACKS_FILENAME))
    {
        error("Not found texture for back");
        exit(0);
    }

    if (!pictures.load_buttons(BUTTONS_FILENAME))
    {
        error("Not found texture for button");
    }
    
    std::vector<Button> buttons = define_buttons(pictures);
    int selected_button = NO_BUTTON;
    int cur_room = mainMenu;
    
    sf::Font font;
    if (!font.loadFromFile(FONT_FILENAME)) {
        error("Not found font file");
    }
    
    int mouseX = 0, mouseY = 0;
    int mouseX_old = 0, mouseY_old = 0;
    
    sf::Clock cursor_timer;
    bool cursor_visible = true;
    
    sf::Clock day_timer;
    
    Experiment experiment;
    ExperimentView experiment_view;
    const CelestialBody *focused_object = nullptr;
    const CelestialBody *draw_trace_object = nullptr;
    
    bool dragging_with_mouse = false;
    bool executing_continuous = false;
    bool mouse_moved = false;
    bool mouse_clicked = false;
    bool showing_info = false;
    
    int creating_comet = noCreatingComet;
    
    int showing_trace_mode = showAllTrace;
    int showing_names_mode = showAllNames;
    bool fullscreen = false;

    while (window.isOpen())
    {
        mouseX = sf::Mouse::getPosition(window).x;
        mouseY = sf::Mouse::getPosition(window).y;
        int mouse_dx = mouseX - mouseX_old;
        int mouse_dy = mouseY - mouseY_old;
        
        if (vector_length(mouse_dx, mouse_dy) >= MOUSE_MAX_DIST_FOR_IDLE) {
            mouse_moved = true;
            selected_button = NO_BUTTON;
            
            if (dragging_with_mouse) {
                experiment_view.move(mouse_dx, mouse_dy);
            }
            mouseX_old = mouseX;
            mouseY_old = mouseY;
        }
        
        int old_log_size = log.size();
        bool buttons_visible = !showing_info && (creating_comet == noCreatingComet);
        bool comet_input_active = creating_comet > choosingCometDestinationPos;
        bool going_to_start = false;

        //handling events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
                
            if (event.type == sf::Event::MouseButtonReleased) {
                log_scrolling = false;
                info_scrolling = false;
                detailed_info_scrolling = false;
                dragging_with_mouse = false;
                
                //mouse click: pressing and releasing at the same place
                if (!mouse_moved) {
                    mouse_clicked = true;
                }
            }
            
            if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left) {
                mouse_moved = false;
                selected_button = NO_BUTTON;
                if (buttons_visible) {
                    for (auto &but: buttons) {
                        if (cur_room == but.room && but.is_touched(mouseX, mouseY)) {
                            selected_button = but.name;
                            break;
                        }
                    }
                }
                
                if (cur_room == gameRoom && selected_button == NO_BUTTON && !showing_info) {
                    focused_object = nullptr;
                    if (!log_field.is_touched(mouseX, mouseY)) {
                        dragging_with_mouse = true;
                    }
                }
                
                if (cur_room == gameRoom) {
                    if (log_field.is_button_touched(mouseX, mouseY)) {
                        log_scrolling = true;
                    }
                    
                    if (info_field.is_button_touched(mouseX, mouseY)) {
                        info_scrolling = true;
                    }
                    
                    if (detailed_info_field.is_button_touched(mouseX, mouseY)) {
                        detailed_info_scrolling = true;
                    }
                
                    if (showing_info && !(info_field.is_touched(mouseX, mouseY))) {
                        showing_info = false;
                    }
                }
            }

            if (event.type == sf::Event::MouseWheelScrolled &&
                    event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel) {
                if (cur_room == gameRoom) {
                    if (showing_info && info_field.is_touched(mouseX, mouseY)) {
                        if (event.mouseWheelScroll.delta > 0) {
                            info_field.scroll_up();
                        } else {
                            info_field.scroll_down();
                        }
                    } else if (focused_object && detailed_info_field.is_touched(mouseX, mouseY)) {
                        if (event.mouseWheelScroll.delta > 0) {
                            detailed_info_field.scroll_up();
                        } else {
                            detailed_info_field.scroll_down();
                        }
                    } else if (log_field.is_touched(mouseX, mouseY)) {
                        if (event.mouseWheelScroll.delta > 0) {
                            log_field.scroll_up();
                        } else {
                            log_field.scroll_down();
                        }
                    } else {
                        if (event.mouseWheelScroll.delta < 0) {
                            experiment_view.zoom_in(mouseX, mouseY);
                        } else {
                            experiment_view.zoom_out(mouseX, mouseY);
                        }
                    }
                }
            }
            
            if (event.type == sf::Event::KeyPressed) {
                auto key = event.key.code;
                //TODO comet text fields
                if (cur_room == inputRoom) {
                    switch (key) {
                        case sf::Keyboard::Left:
                            filepath_input_field.left();
                            break;
                        case sf::Keyboard::Right:
                            filepath_input_field.right();
                            break;
                        case sf::Keyboard::End:
                            filepath_input_field.end();
                            break;
                        case sf::Keyboard::Home:
                            filepath_input_field.home();
                            break;
                        case sf::Keyboard::Delete:
                            filepath_input_field.del();
                            break;
                        case sf::Keyboard::Enter:
                            filepath = filepath_input_field.get_string();
                            going_to_start = true;
                            break;
                        default:
                            break;
                    }
                } else if (cur_room == gameRoom) {
                    switch (key) {
                        case sf::Keyboard::Left:
                            comet_input_field.left();
                            break;
                        case sf::Keyboard::Right:
                            comet_input_field.right();
                            break;
                        case sf::Keyboard::End:
                            comet_input_field.end();
                            break;
                        case sf::Keyboard::Home:
                            comet_input_field.home();
                            break;
                        case sf::Keyboard::Delete:
                            comet_input_field.del();
                            break;
                        default:
                            break;
                    }
                }
                
                if (cur_room == gameRoom) {
                    switch (key) {
                        case sf::Keyboard::Home:
                            experiment_view.clear();
                            focused_object = nullptr;
                            break;
                        case sf::Keyboard::Escape:
                            showing_info = false;
                            creating_comet = noCreatingComet;
                            break;
                        case sf::Keyboard::Tab:
                            if (fullscreen) {
                                window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), app_name);
                            } else {
                                window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), app_name, sf::Style::Fullscreen);
                            }
                            fullscreen = !fullscreen;
                            break;
                        case sf::Keyboard::Enter:
                            if (creating_comet == enteringCometWeight) {
                                try {
                                    comet_weight = stod(comet_input_field.get_string());
                                    creating_comet = enteringCometSpeed;
                                    comet_input_field.set_string(default_comet_speed);
                                } catch (std::invalid_argument s) {
                                    log.push_back("Invalid value for comet weight!");
                                    continue;
                                }
                            } else if (creating_comet == enteringCometSpeed) {
                                try {
                                    comet_speed = stod(comet_input_field.get_string());
                                    calculate_vx_vy(comet_second_x - comet_x, comet_second_y - comet_y,
                                        comet_speed, comet_speed_x, comet_speed_y);
                                    creating_comet = enteringCometRadius;
                                    comet_input_field.set_string(default_comet_radius);
                                } catch (std::invalid_argument s) {
                                    log.push_back("Invalid value for comet speed!");
                                    continue;
                                }
                            } else if (creating_comet == enteringCometRadius) {
                                try {
                                    comet_radius = stod(comet_input_field.get_string());
                                    creating_comet = enteringCometName;
                                    comet_input_field.set_string(default_comet_name);
                                } catch (std::invalid_argument s) {
                                    log.push_back("Invalid value for comet radius!");
                                    continue;
                                }
                            } else if (creating_comet == enteringCometName) {
                                comet_name = comet_input_field.get_string();
                                experiment.add_object(new Comet(comet_name, comet_weight,
                                    comet_radius, comet_x, comet_y, comet_speed_x, comet_speed_y));
                                creating_comet = noCreatingComet;
                                log.push_back("New comet " + comet_name + " added!");
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            
            if (event.type == sf::Event::TextEntered) {
                if (cur_room == inputRoom) {
                    char ch = static_cast<char>(event.text.unicode);
                    if (ch == '\b') {
                        filepath_input_field.backspace();
                    } else if (!isspace(ch) && !sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
                        filepath_input_field.put_char(ch);
                    }
                }
                if (cur_room == gameRoom) {
                    if (comet_input_active) {
                        char ch = static_cast<char>(event.text.unicode);
                        if (ch == '\b') {
                            comet_input_field.backspace();
                        } else if (!isspace(ch) && !sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
                            comet_input_field.put_char(ch);
                        }
                    }
                }
            }
        }
        
        if (mouse_clicked) {
            mouse_clicked = false;
            
            if (cur_room == inputRoom && filepath_input_field.is_touched(mouseX, mouseY)) {
                filepath_input_field.set_position(mouseX);
            }
            
            if (cur_room == gameRoom && selected_button == NO_BUTTON) {
                if (showing_info && info_field.is_touched(mouseX, mouseY)) {
                    int row = info_field.get_touched_position(mouseY);
                    if (row >= 3 && row < info_object_ptrs.size()) {
                        focused_object = info_object_ptrs[row];
                        draw_trace_object = focused_object;
                        showing_info = false;
                    }
                }
                
                if (creating_comet == choosingCometStartPos) {
                    experiment_view.get_local_position(mouseX, mouseY, comet_x, comet_y);
                    creating_comet = choosingCometDestinationPos;
                } else if (creating_comet == choosingCometDestinationPos) {
                    experiment_view.get_local_position(mouseX, mouseY, comet_second_x, comet_second_y);
                    if (vector_length(comet_second_x - comet_x, comet_second_y - comet_y) > EPS) {
                        comet_input_field.set_string(default_comet_weight);
                        creating_comet = enteringCometWeight;
                    }
                }
            }
            
            switch (selected_button) {
                //mainMenu
                case quitGame:
                    window.close();
                    break;
                case toInstr:
                    cur_room = instrRoom;
                    break;
                case startGame:
                    if (filepath == "") {
                        filepath = default_filepath;
                    }
                    filepath_input_field.set_string(filepath);
                    cur_room = inputRoom;
                    break;
                
                //inputRoom
                case typingCancel:
                    cur_room = mainMenu;
                    break;
                case typingClear:
                    filepath_input_field.set_string("");
                    break;
                case typingOK:
                    filepath = filepath_input_field.get_string();
                    going_to_start = true;
                    break;
                
                //gameRoom
                case toMainMenu:
                    log_field.resize(0);
                    log.clear();
                    cur_room = mainMenu;
                    break;
                case executeStep:
                    executing_continuous = false;
                    experiment.next_day(log);
                    day_timer.restart();
                    break;
                case executeAll:
                    executing_continuous = true;
                    log.push_back("Continuous execution.");
                    log.push_back("");
                    break;
                case executePause:
                    executing_continuous = false;
                    log.push_back("Execution paused.");
                    log.push_back("");
                    break;
                case executeSpeed1:
                    experiment.set_days_per_second(1);
                    executing_continuous = true;
                    log.push_back("Speed: 1 day per second.");
                    log.push_back("");
                    break;
                case executeSpeed4:
                    experiment.set_days_per_second(4);
                    executing_continuous = true;
                    log.push_back("Speed: 4 days per second.");
                    log.push_back("");
                    break;
                case executeSpeed16:
                    experiment.set_days_per_second(16);
                    executing_continuous = true;
                    log.push_back("Speed: 16 days per second.");
                    log.push_back("");
                    break;
                case executeSpeed64:
                    experiment.set_days_per_second(64);
                    executing_continuous = true;
                    log.push_back("Speed: 64 days per second.");
                    log.push_back("");
                    break;
                case executeSpeed365:
                    experiment.set_days_per_second(365);
                    executing_continuous = true;
                    log.push_back("Speed: 365 days per second!");
                    log.push_back("");
                    break;
                case showInfo:
                    showing_info = true;
                    executing_continuous = false;
                    break;
                case addComet:
                    creating_comet = choosingCometStartPos;
                    executing_continuous = false;
                    break;
                case switchShowingOrbits:
                    if (showing_trace_mode == showAllTrace) {
                        showing_trace_mode = showOneTrace;
                        log.push_back("Trace: only selected object.");
                    } else if (showing_trace_mode == showOneTrace) {
                        showing_trace_mode = showNoTrace;
                        log.push_back("Trace: nothing.");
                    } else {
                        showing_trace_mode = showAllTrace;
                        log.push_back("Trace: all.");
                    }
                    break;
                case switchShowingNames:
                    if (showing_names_mode == showAllNames) {
                        showing_names_mode = showPlanetStarsNames;
                        log.push_back("Names: planets and stars.");
                    } else if (showing_names_mode == showPlanetStarsNames) {
                        showing_names_mode = showNoNames;
                        log.push_back("Names: nothing.");
                    } else {
                        showing_names_mode = showAllNames;
                        log.push_back("Names: all.");
                    }
                    break;
                
                //instrRoom
                case backInstr:
                    cur_room = mainMenu;
                    break;
                
                default:
                    break;
            }
            selected_button = NO_BUTTON;
        }

        if (going_to_start) {
            going_to_start = false;
            log.clear();
            if (experiment.load_from_file(filepath, log)) {
                log_field.resize(log.size());
                log_field.scroll_to_top();
                
                executing_continuous = false;
                experiment_view.clear();
                focused_object = nullptr;
                draw_trace_object = nullptr;
                cur_room = gameRoom;
                creating_comet = noCreatingComet;
            }
        }
        
        if (cur_room == gameRoom && executing_continuous) {
            if (day_timer.getElapsedTime() > sf::milliseconds(1000 / (experiment.get_days_per_second()))) {
                experiment.next_day(log);
                day_timer.restart();
            }
        }
        
        //DRAWING
        window.clear(sf::Color::White);
        
        //drawing background
        window.draw(pictures.backs[cur_room]);
        
        if (cur_room == gameRoom) {
            //drawing orbits
            if (showing_trace_mode == showAllTrace) {
                for (int i = 0; i < experiment.size(); ++i) {
                    draw_trace(window, experiment.objects(i), experiment_view);
                }
            } else if (showing_trace_mode == showOneTrace && draw_trace_object) {
                draw_trace(window, draw_trace_object, experiment_view);
            }
            
            //drawing objects
            show_experiment(window, experiment, experiment_view, font, showing_names_mode);
            
            //drawing detailed info about object
            if (focused_object && !showing_info) {
                double x = focused_object->x;
                double y = focused_object->y;
                experiment_view.focus_on_target(x, y);
                draw_object_info(window, experiment, focused_object, experiment_view, font, detailed_info_field);
            }
            
            //log updating
            log_field.resize(log.size());
            if (log.size() != old_log_size) {
                log_field.scroll_to_bottom();
            }
            
            //expired days
            sf::Text text;
            text.setFont(font);
            text.setFillColor(sf::Color::White);
            text.setCharacterSize(40);
            text.setString(std::to_string(experiment.get_expired_days()) + " day");
            text.setPosition(1050, 20);
            window.draw(text);
            
            //drawing info about planet system
            if (showing_info) {
                draw_experiment_info(window, experiment, font, info_field, info_object_ptrs);
            } else {
                //drawing log
                log_field.draw(window, log, font);
            }
            
            if (creating_comet != noCreatingComet) {
                int sign_x = 20;
                std::string sign;
                switch (creating_comet) {
                    case choosingCometStartPos:
                        sign = "[1/6] Click on place where you want to create the comet (ESC for cancel):";
                        break;
                    case choosingCometDestinationPos:
                        sign = "[2/6] Click on place to indicate comet direction (ESC for cancel):";
                        break;
                    case enteringCometWeight:
                        sign = "[3/6] Enter comet weight (kg) and press ENTER (ESC for cancel):";
                        break;
                    case enteringCometSpeed:
                        sign = "[4/6] Enter comet speed (m/s) and press ENTER (ESC for cancel):";
                        break;
                    case enteringCometRadius:
                        sign = "[5/6] Enter comet radius (m) and press ENTER (ESC for cancel):";
                        break;
                    case enteringCometName:
                        sign = "[6/6] Enter comet name and press ENTER (ESC for cancel):";
                        break;
                }
                
                text.setFillColor(sf::Color::White);
                text.setCharacterSize(24);
                text.setString(sign);
                text.setPosition(sign_x, 100);
                window.draw(text);
                
                if (creating_comet > choosingCometStartPos) {
                    double screen_x, screen_y;
                    experiment_view.get_screen_position(comet_x, comet_y, screen_x, screen_y);
                    sf::CircleShape circle(5);
                    circle.setFillColor(sf::Color(128, 128, 128, 255));
                    circle.setPosition(screen_x, screen_y);
                    window.draw(circle);
                }
                if (creating_comet > choosingCometDestinationPos) {
                    double screen_x, screen_y;
                    experiment_view.get_screen_position(comet_second_x, comet_second_y, screen_x, screen_y);
                    sf::CircleShape circle(5);
                    circle.setFillColor(sf::Color(128, 128, 128, 255));
                    circle.setPosition(screen_x, screen_y);
                    window.draw(circle);
                }
                
                if (comet_input_active) {
                    comet_input_field.draw(window, font, cursor_visible);
                }
            }
        }
        
        //scrolling
        if (cur_room == gameRoom) {
            if (log_scrolling) {
                log_field.scroll_with_mouse(mouseY);
            }
            if (info_scrolling) {
                log_field.scroll_with_mouse(mouseY);
            }
        }

        //drawing buttons
        if (buttons_visible) {
            for (int i = 0; i < buttons.size(); ++i) {
                if (buttons[i].room == cur_room) {
                    window.draw(pictures.buttons[i]);
                }
            }
            
            if (selected_button != NO_BUTTON) {
                Button but = buttons[selected_button];
                sf::RectangleShape rectangle(sf::Vector2f(but.dx, but.dy));
                rectangle.setPosition(but.x, but.y);
                rectangle.setFillColor(sf::Color(0, 0, 0, 60));
                window.draw(rectangle);
            }
        }
        

        //drawing filepath field
        if (cur_room == inputRoom) {
            filepath_input_field.draw(window, font, cursor_visible);
        }
        
        if (cur_room == inputRoom || cur_room == gameRoom) {
            if (cursor_visible) {
                if (cursor_timer.getElapsedTime() > sf::milliseconds(CURSOR_TIME_VISIBLE_MSEC)) {
                    cursor_timer.restart();
                    cursor_visible = false;
                }
            } else {
                if (cursor_timer.getElapsedTime() > sf::milliseconds(CURSOR_TIME_INVISIBLE_MSEC)) {
                    cursor_timer.restart();
                    cursor_visible = true;
                }
            }
        }

        window.display();
    }

    return 0;
}
