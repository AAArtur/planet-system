#include "physics.h"

//FUNCTIONS

//divide given string by space characters: " Earth 10e-10  9e-9 " -> ("Earth", "10e-10", "9e-9")
std::vector<std::string> split(std::string str)
{
    std::vector<std::string> list{};
    auto left = str.begin();
    auto right = left;
    while (right != str.end()) {
        while (isspace(*left) && left != str.end()) {
            ++left;
        }
        if (left == str.end()) {
            break;
        }
        right = left;
        while (!isspace(*right) && right != str.end()) {
            ++right;
        }
        list.push_back(std::string(left, right));
        left = right;
    }
    return list;
}

// ("VeryLongString", 9) -> "VeryLon.."
std::string short_name(std::string name, int i)
{
    int len = name.size();
    if (len < i) {
        return name;
    }
    
    if (i >= 6) {
        return name.substr(0, i - 2) + "..";
    } else {
        return name.substr(0, i);
    }
}

//calculates r^2
double dist2(const CelestialBody *b1, const CelestialBody *b2) {
    double dx = b1->x - b2->x;
    double dy = b1->y - b2->y;
    return dx * dx + dy * dy;
}

//calculates r
double dist(const CelestialBody *b1, const CelestialBody *b2) {
    return sqrt(dist2(b1, b2));
}

//calculates F = G * m1 * m2 / r^2
double gravity_force(const CelestialBody *b1, const CelestialBody *b2)
{
    double r2 = dist2(b1, b2);
    return G * b1->weight * b2->weight / r2;
}

//calculates F' = G / r^2, to calculate later a = F / m1 = F' * m2
double gravity_force_part(const CelestialBody *b1, const CelestialBody *b2)
{
    double r2 = dist2(b1, b2);
    return G / r2;
}

int signum(double d)
{
    if (d > EPS) {
        return 1;
    }
    if (d < -EPS) {
        return -1;
    }
    return 0;
}

//finds projections of vector to the X/Y-axis
void calculate_vx_vy(double dx, double dy, double v, double &vx, double &vy)
{
    int sign_dx = signum(dx);
    int sign_dy = signum(dy);
    
    vx = v / sqrt(1 + (dy / dx) * (dy / dx)) * sign_dx;
    vy = v / sqrt(1 + (dx / dy) * (dx / dy)) * sign_dy;
}

//a1 = F / m1 = F' * m2, a2 = F / m2 = F' * m1
void calculate_acceleration(const CelestialBody *b1, const CelestialBody *b2,
    double &ax1, double &ay1, double &ax2, double &ay2)
{
    double dx = b2->x - b1->x;
    double dy = b2->y - b1->y;
    double f_part = gravity_force_part(b1, b2);
    double a1 = f_part * b2->weight;
    double a2 = f_part * b1->weight;
    calculate_vx_vy(dx, dy, a1, ax1, ay1);
    calculate_vx_vy(-dx, -dy, a2, ax2, ay2);
}

double vector_length(double x, double y)
{
    return sqrt(x * x + y * y);
}

//unite two CelestialBody'es into one, s1 and s2 are log messages
CelestialBody *collapse_objects(const CelestialBody *b1, const CelestialBody *b2, std::string &s1, std::string &s2)
{
    double w1 = b1->weight, w2 = b2->weight;
    double r1 = b1->radius, r2 = b2->radius;
    double x1 = b1->x, x2 = b2->x;
    double y1 = b1->y, y2 = b2->y;
    double sx1 = b1->speed_x, sx2 = b2->speed_x;
    double sy1 = b1->speed_y, sy2 = b2->speed_y;
    std::string name1 = b1->name, name2 = b2->name;
    
    std::string new_name = name1 + "&" + name2;
    double new_weight = w1 + w2;
    double new_radius = pow(r1 * r1 * r1 + r2 * r2 * r2, 1.0 / 3.0);
    double new_x = (x1 * r1 + x2 * r2) / (r1 + r2);
    double new_y = (y1 * r1 + y2 * r2) / (r1 + r2);
    double new_speed_x = (w1 * sx1 + w2 * sx2) / (w1 + w2);
    double new_speed_y = (w1 * sy1 + w2 * sy2) / (w1 + w2);
    
    std::string short_name1 = short_name(name1, 10), short_name2 = short_name(name2, 10);
    std::string short_new_name = short_name(new_name, 18);
    CelestialBody *object;
    
    if (b1->type == objectStar && b2->type == objectStar) {
        s1 = "Stars " + short_name1 + " and " + short_name2 + " collapsed";
        s2 = "into new star called " + short_new_name;
        object = new Star(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else if (b1->type == objectStar && b2->type != objectStar) {
        s1 = short_name2 + " smashed into star " + short_name1 + ",";
        s2 = "now it called " + short_new_name;
        object = new Star(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else if (b1->type != objectStar && b2->type == objectStar) {
        s1 = short_name1 + " smashed into star " + short_name2 + ",";
        s2 = "now it called " + short_new_name;
        object = new Star(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else if (b1->type == objectPlanet && b2->type == objectPlanet) {
        s1 = "Planets " + short_name1 + " and " + short_name2 + " collapsed";
        s2 = "into new planet called " + short_new_name;
        object = new Planet(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else if (b1->type == objectPlanet && b2->type != objectPlanet) {
        s1 = short_name2 + " falled on planet " + short_name1 + ",";
        s2 = "new planet called " + short_new_name;
        object = new Planet(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else if (b1->type != objectPlanet && b2->type == objectPlanet) {
        s1 = short_name1 + " falled on planet " + short_name2 + ",";
        s2 = "new planet called " + short_new_name;
        object = new Planet(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else if (b1->type == objectSatellite && b2->type == objectSatellite) {
        s1 = short_name1 + " and " + short_name2 + " collapsed";
        s2 = "into new satellite called " + short_new_name;
        object = new Satellite(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else if (b1->type == objectSatellite && b2->type != objectSatellite) {
        s1 = short_name2 + " falled on satellite " + short_name1 + ",";
        s2 = "new satellite called " + short_new_name;
        object = new Satellite(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else if (b1->type != objectSatellite && b2->type == objectSatellite) {
        s1 = short_name1 + " falled on satellite " + short_name2 + ",";
        s2 = "new satellite called " + short_new_name;
        object = new Satellite(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    } else {
        s1 = "Comets " + short_name1 + " and " + short_name2 + " collapsed";
        s2 = "into new comet called " + short_new_name;
        object = new Comet(new_name, new_weight, new_radius, new_x, new_y, new_speed_x, new_speed_y);
    }
    
    return object;
}


//class Experiment

Experiment::Experiment()
{
    days_per_second = DAYS_PER_SECOND_DEFAULT;
    object_system = {};
    days = 0;
}

int Experiment::get_expired_days() const
{
    return days;
}

void Experiment::set_days_per_second(int d)
{
    days_per_second = d;
}

int Experiment::get_days_per_second() const
{
    return days_per_second;
}

int Experiment::size() const
{
    return object_system.size();
}

const CelestialBody *Experiment::objects(int i) const
{
    return object_system.objects(i);
}

void Experiment::clear()
{
    days_per_second = DAYS_PER_SECOND_DEFAULT;
    object_system = {};
    days = 0;
}

void Experiment::next_day(std::vector<std::string> &log)
{
    object_system.next_day(log, days);
    ++days;
}

void Experiment::add_object(CelestialBody *object)
{
    object_system.add_object(object);
}

bool Experiment::load_from_file(std::string filename, std::vector<std::string> &log)
{
    this->clear();
    std::ifstream file(filename);
    if (!file.is_open()) {
        return false;
    }
    log.push_back("Loading data from file:");
    log.push_back("\"" + filename + "\"");
    
    std::string str;
    while (getline(file, str)) {
        std::vector<std::string> args = split(str);
        if (args.size() == 0 || str.size() == 0 || str[0] == '#') {
            continue;
        }
        
        std::string args_size_str = std::to_string(args.size());
        if (args.size() < 8) {
            log.push_back("Too few arguments, need 8 at least, found " + args_size_str + ":");
            log.push_back("\"" + str + "\"");
        } else {
            std::string name = args[1];
            double w, r, x, y, speed_x, speed_y;
            try {
                w = stod(args[2]);
                r = stod(args[3]);
                x = stod(args[4]);
                y = stod(args[5]);
                speed_x = stod(args[6]);
                speed_y = stod(args[7]);
            } catch (std::invalid_argument s) {
                log.push_back("Something get wrong: " + name);
                continue;
            }
            if (args[0] == "planet") {
                if (args.size() != 8) {
                    log.push_back("Expected 8 arguments for \"planet\", found " + args_size_str + ":");
                    log.push_back(str);
                } else {
                    object_system.add_object(new Planet(name, w, r, x, y, speed_x, speed_y));
                    log.push_back("+ planet " + name);
                }
            } else if (args[0] == "star") {
                if (args.size() != 8) {
                    log.push_back("Expected 8 arguments for \"star\", found " + args_size_str + ":");
                    log.push_back(str);
                } else {
                    object_system.add_object(new Star(name, w, r, x, y, speed_x, speed_y));
                    log.push_back("+ star " + name);
                }
            } else if (args[0] == "comet") {
                if (args.size() != 8) {
                    log.push_back("Expected 8 arguments for \"comet\", found " + args_size_str + ":");
                    log.push_back(str);
                } else {
                    object_system.add_object(new Comet(name, w, r, x, y, speed_x, speed_y));
                    log.push_back("+ comet " + name);
                }
            } else if (args[0] == "satellite") {
                if (args.size() != 9) {
                    log.push_back("Expected 9 arguments for \"satellite\", found " + args_size_str + ":");
                    log.push_back(str);
                } else {
                    bool success = false;
                    std::string parent_name = args[8];
                    for (int i = 0; i < object_system.size(); ++i) {
                        if (object_system.objects(i)->name == parent_name) {
                            const CelestialBody *p = object_system.objects(i);
                            object_system.add_object(new Satellite(name, w, r,
                                p->x + x, p->y + y, p->speed_x + speed_x, p->speed_y + speed_y));
                            log.push_back("+ satellite " + name + "(" + parent_name + ")");
                            success = true;
                            break;
                        }
                    }
                    if (!success) {
                        log.push_back("Cannot find parent object with");
                        log.push_back("name " + parent_name + "for " + name);
                    }
                }
            } else {
                log.push_back("Unknown object type: " + args[0]);
            }
        }
    }
    
    log.push_back("Loaded from file successfully!");
    log.push_back("");
    return true;
}


//class ObjectSystem

ObjectSystem::ObjectSystem() {};

const CelestialBody *ObjectSystem::objects(int i) const
{
    return alive_objects[i].get();
}

int ObjectSystem::size() const
{
    return alive_objects.size();
}

void ObjectSystem::add_object(CelestialBody *object)
{
    alive_objects.push_back(std::shared_ptr<CelestialBody>(object));
}

void ObjectSystem::next_day(std::vector<std::string> &log, int day_number)
{
    for (int t = 0; t < TICKS_PER_DAY; ++t) {
        int n1 = 0, n2;
        while (n1 < alive_objects.size()) {
            n2 = n1 + 1;
            while (n2 < alive_objects.size()) {
                const CelestialBody *b1 = this->objects(n1);
                const CelestialBody *b2 = this->objects(n2);
                //constructing new object if some objects collapsed into one
                if (dist(b1, b2) < (b1->radius + b2->radius)) {
                    std::string s1, s2;
                    CelestialBody *object = collapse_objects(b1, b2, s1, s2);
                    this->add_object(object);
                    log.push_back("[Day " + std::to_string(day_number) + "] " + s1);
                    log.push_back(s2);
                    log.push_back("");
                    
                    dead_objects.push_back(alive_objects[n1]);
                    dead_objects.push_back(alive_objects[n2]);
                    
                    alive_objects.erase(alive_objects.begin() + n2);
                    alive_objects.erase(alive_objects.begin() + n1);
                    n1 = -1;
                    break;
                }
                ++n2;
            }
            ++n1;
        }
        
        int s = alive_objects.size();
        std::vector<double> ax(s);
        std::vector<double> ay(s);
        for (int i = 0; i < s; ++i) {
            ax[i] = 0;
            ay[i] = 0;
        }
        
        for (int i = 0; i < s; ++i) {
            for (int j = i + 1; j < s; ++j) {
                double ax1, ay1, ax2, ay2;
                calculate_acceleration(alive_objects[i].get(), alive_objects[j].get(), ax1, ay1, ax2, ay2);
                ax[i] += ax1;
                ay[i] += ay1;
                ax[j] += ax2;
                ay[j] += ay2;
            }
        }
        
        int seconds_per_tick = SECONDS_IN_DAY / TICKS_PER_DAY;
        for (int i = 0; i < s; ++i) {
            alive_objects[i]->x += alive_objects[i]->speed_x * seconds_per_tick;
            alive_objects[i]->y += alive_objects[i]->speed_y * seconds_per_tick;
            
            alive_objects[i]->speed_x += ax[i] * seconds_per_tick;
            alive_objects[i]->speed_y += ay[i] * seconds_per_tick;
        }
    }
    for (int i = 0; i < alive_objects.size(); ++i) {
        alive_objects[i]->points.push_back(std::pair<double, double>(alive_objects[i]->x, alive_objects[i]->y));
    }
}


//struct CelestialBody

CelestialBody::CelestialBody(std::string fname, double w, double r, double px, double py, double sx, double sy) :
    name(fname), weight(w), radius(r), x(px), y(py), speed_x(sx), speed_y(sy), type(objectUnknown), points() {}
CelestialBody::~CelestialBody() {};

//struct Planet

Planet::Planet(std::string fname, double w, double r, double px, double py, double sx, double sy) :
    CelestialBody(fname, w, r, px, py, sx, sy)
{
    type = objectPlanet;
}
Planet::~Planet() {};

//struct Star

Star::Star(std::string fname, double w, double r, double px, double py, double sx, double sy) :
    CelestialBody(fname, w, r, px, py, sx, sy)
{
    type = objectStar;
}
Star::~Star() {};

//struct Comet

Comet::Comet(std::string fname, double w, double r, double px, double py, double sx, double sy) :
    CelestialBody(fname, w, r, px, py, sx, sy)
{
    type = objectComet;
}
Comet::~Comet() {};

//struct Satellite

Satellite::Satellite(std::string fname, double w, double r, double px, double py, double sx, double sy) :
    CelestialBody(fname, w, r, px, py, sx, sy)
{
    type = objectSatellite;
}
Satellite::~Satellite() {};