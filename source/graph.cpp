#include "graph.h"


//FILENAMES

std::vector<std::string> back_filenames()
{
    std::vector<std::string> names(ROOM_MAX);
    names[mainMenu] = "mainMenu";
    names[gameRoom] = "gameRoom";
    names[instrRoom] = "instrRoom";
    names[inputRoom] = "inputRoom";

    return names;
}

std::vector<std::string> button_filenames()
{
    std::vector<std::string> names(BUTTON_MAX);
    names[startGame] = "startGame";
    names[toMainMenu] = "toMainMenu";
    names[quitGame] = "quitGame";
    names[toInstr] = "toInstr";
    
    names[executeStep] = "executeStep";
    names[executeAll] = "executeAll";
    names[executePause] = "executePause";
    names[executeSpeed1] = "executeSpeed1";
    names[executeSpeed4] = "executeSpeed4";
    names[executeSpeed16] = "executeSpeed16";
    names[executeSpeed64] = "executeSpeed64";
    names[executeSpeed365] = "executeSpeed365";
    names[showInfo] = "showInfo";
    names[addComet] = "addComet";
    names[switchShowingNames] = "switchShowingNames",
    names[switchShowingOrbits] = "switchShowingOrbits";
    
    names[typingOK] = "typingOK";
    names[typingClear] = "typingClear";
    names[typingCancel] = "typingCancel";
    
    names[backInstr] = "backInstr";

    return names;
}


//LOCAL FUNCTIONS

bool load_pictures(std::vector<std::shared_ptr<sf::Texture>> &textures,
    std::vector<sf::Sprite> &sprites, std::vector<std::string> filenames, std::string filepath)
{
    textures.clear();
    textures.resize(filenames.size());
    sprites.clear();
    sprites.resize(filenames.size());

    for (int i = 0; i < filenames.size(); ++i) {
        sf::Texture tmp_texture;
        if (!tmp_texture.loadFromFile(filepath + filenames[i] + ".png")) {
            return false;
        }
        textures[i] = std::make_shared<sf::Texture>(tmp_texture);
        sprites[i] = sf::Sprite(*textures[i]);
    }

    return true;
}

bool if_point_hits_rectangle(int px, int py, int x, int y, int dx, int dy)
{
    return (px >= x && px <= x + dx && py >= y && py <= y + dy);
}


//struct GraphStorage
GraphStorage::GraphStorage() {}

bool GraphStorage::load_backs(std::string filepath)
{
    return load_pictures(back_textures, backs, back_filenames(), filepath);
}

bool GraphStorage::load_buttons(std::string filepath)
{
    return load_pictures(button_textures, buttons, button_filenames(), filepath);
}


//struct Button
Button::Button(int fx, int fy, int fdx, int fdy, int fname, int froom) :
    x(fx), y(fy), dx(fdx), dy(fdy), name(fname), room(froom) {}

Button::Button() :
    x(0), y(0), dx(0), dy(0), name(NO_BUTTON), room(NO_ROOM) {}

bool Button::is_touched(int mouse_x, int mouse_y) const
{
    return if_point_hits_rectangle(mouse_x, mouse_y, x, y, dx, dy);
}


//class TextField
TextField::TextField(int fx, int fy, int fdx, int fdy, int ffont_size, int fmax_size) :
    x(fx), y(fy), dx(fdx), dy(fdy), font_size(ffont_size), max_size(fmax_size)
{
    content = "";
    pos = 0;
    font_width = font_size * FONT_WIDTH_COEF;
}

std::string TextField::get_string() const
{
    return content;
}

void TextField::put_char(char ch)
{
    content.insert(content.begin() + pos, ch);
    ++pos;
}

void TextField::clear()
{
    content = "";
    pos = 0;
}

void TextField::right()
{
    if (pos < content.size()) {
        ++pos;
    }
}

void TextField::left()
{
    if (pos > 0) {
        --pos;
    }
}

void TextField::end()
{
    pos = content.size();
}

void TextField::home()
{
    pos = 0;
}

void TextField::del()
{
    if (pos < content.size()) {
        content.erase(content.begin() + pos);
    }
}

void TextField::backspace()
{
    if (pos > 0) {
        content.erase(content.begin() + pos - 1);
        --pos;
    }
}

void TextField::set_string(std::string str)
{
    content = str;
    pos = str.size();
}

bool TextField::is_touched(int mouse_x, int mouse_y) const
{
    return if_point_hits_rectangle(mouse_x, mouse_y, x, y, dx, dy);
}

void TextField::set_position(int mouse_x)
{
    pos = this->get_touched_position(mouse_x);
}

void TextField::draw(sf::RenderWindow &window, sf::Font &font, bool draw_cursor) const
{
    sf::RectangleShape back(sf::Vector2f(dx, dy));
    back.setFillColor(TEXT_BACKGROUND_COLOR);
    back.setPosition(x, y);
    window.draw(back);
    
    sf::Text text;
    text.setFont(font);
    text.setFillColor(APP_TEXT_COLOR);
    text.setCharacterSize(font_size);
    text.setString(content);
    text.setPosition(this->text_screen_x(), this->text_screen_y());
    window.draw(text);
    
    if (draw_cursor) {
        float cursor_dx = font_size * 0.08;
        float cursor_dy = std::min(float(dy), font_size * 1.2f);
        sf::RectangleShape cursor(sf::Vector2f(cursor_dx, cursor_dy));
        cursor.setFillColor(APP_TEXT_COLOR);
        int draw_x = this->text_screen_x() + pos * font_width;
        int draw_y = y + (dy - cursor_dy) / 2;
        cursor.setPosition(draw_x, draw_y);
        window.draw(cursor);
    }
}

int TextField::text_screen_x() const
{
    return x + dx / 2 - 1.0 * content.size() / 2 * font_width;
}

int TextField::text_screen_y() const
{
    return y + dy / 2 - font_size * 0.6;
}

int TextField::get_touched_position(int mouse_x) const
{
    int new_pos = (mouse_x - this->text_screen_x() + font_width * 0.5) / font_width;
    return std::max(0, std::min(new_pos, int(content.size())));
}


//struct ScrollField
ScrollField::ScrollField(int fx, int fy, int fdx, int fdy, int ffont_size, int ffont_gap) :
    x(fx), y(fy), dx(fdx), dy(fdy), font_size(ffont_size), font_gap(ffont_gap)
{
    size = 0;
    pixels_per_scroll = font_size / 2;
    scrolling = 0;
    max_scrolling = this->get_max_scrolling();
    
    this->update_button();
    font_width = font_size * FONT_WIDTH_COEF;
    text_offset_y = font_size * 0.5;
}

void ScrollField::scroll_up()
{
    scrolling -= 1;
    if (scrolling < 0) {
        scrolling = 0;
    }
    this->update_button();
}

void ScrollField::scroll_down()
{
    scrolling += 1;
    if (scrolling > max_scrolling) {
        scrolling = max_scrolling;
    }
    this->update_button();
}

void ScrollField::scroll_with_mouse(int mouse_y)
{
    int min_y = y;
    int max_y = y + dy - button_dy;
    if (max_y <= min_y) {
        return;
    }
    int my = std::min(max_y, std::max(min_y, mouse_y - button_dy / 2));
    scrolling = max_scrolling * (my - min_y) / (max_y - min_y);
}

void ScrollField::scroll_to_top()
{
    scrolling = 0;
}

void ScrollField::scroll_to_bottom()
{
    scrolling = max_scrolling;
}

void ScrollField::resize(int new_size)
{
    if (size != new_size) {
        size = new_size;
        max_scrolling = this->get_max_scrolling();
        scrolling = std::min(scrolling, max_scrolling);
    }
}

bool ScrollField::is_touched(int mouse_x, int mouse_y) const
{
    return if_point_hits_rectangle(mouse_x, mouse_y, x, y, dx, dy);
}

bool ScrollField::is_button_touched(int mouse_x, int mouse_y)
{
    this->update_button();
    return if_point_hits_rectangle(mouse_x, mouse_y, button_x, button_y, button_dx, button_dy);
}

int ScrollField::top_visible_line() const
{
    return std::max(0, (int(scrolling) * pixels_per_scroll - text_offset_y) / (font_size + font_gap));
}

int ScrollField::bottom_visible_line() const
{
    return std::min(size - 1, this->top_visible_line() + dy / (font_size + font_gap) + 1);
}

int ScrollField::text_screen_x() const
{
    return x + font_width * 0.5;
}

int ScrollField::text_screen_y(int i) const
{
    return y + text_offset_y + i * (font_size + font_gap) - scrolling * pixels_per_scroll;
}

int ScrollField::get_touched_position(int mouse_y) const
{
    return (mouse_y - y - text_offset_y + scrolling * pixels_per_scroll) / (font_size + font_gap);
}

float ScrollField::get_max_scrolling() const
{
    return std::max(0, (size * (font_size + font_gap) - dy + dy / 5) / pixels_per_scroll);
}

void ScrollField::update_button()
{
    button_x = x + dx - button_dx;
    button_dx = std::min(dx, SCROLL_BUTTON_WIDTH);
    button_dy = std::min(dy, std::max(button_dx, int(dy / (dy + max_scrolling * pixels_per_scroll) * dy)));
    if (max_scrolling > 0) {
        button_y = y + (dy - button_dy) * (scrolling / max_scrolling);
    } else {
        button_y = y;
    }
}

void ScrollField::draw(sf::RenderWindow &window, const std::vector<std::string> &strs, const sf::Font &font)
{
    this->update_button();
    sf::RectangleShape back(sf::Vector2f(dx, dy));
    back.setFillColor(TEXT_BACKGROUND_COLOR);
    back.setPosition(x, y);
    window.draw(back);
    
    sf::Text text;
    text.setFont(font);
    text.setFillColor(APP_TEXT_COLOR);
    text.setCharacterSize(font_size);
    int top_line = this->top_visible_line();
    int bottom_line = this->bottom_visible_line();
    int draw_x = this->text_screen_x();
    for (int i = top_line; i <= bottom_line && i < strs.size(); ++i) {
        text.setString(strs[i]);
        int draw_y = this->text_screen_y(i);
        text.setPosition(draw_x, draw_y);
        window.draw(text);
    }
    
    sf::RectangleShape border(sf::Vector2f(dx, font_size));
    border.setFillColor(SCROLL_BORDER_COLOR);
    border.setPosition(x, y - font_size);
    window.draw(border);
    border.setPosition(x, y + dy);
    window.draw(border);
    
    sf::RectangleShape left_border(sf::Vector2f(font_size, dy + 2 * font_size));
    left_border.setFillColor(SCROLL_BORDER_COLOR);
    left_border.setPosition(x - font_size, y - font_size);
    window.draw(left_border);
    
    sf::RectangleShape scroll_line(sf::Vector2f(button_dx, dy));
    scroll_line.setFillColor(SCROLL_LINE_COLOR);
    scroll_line.setPosition(x + dx - button_dx, y);
    window.draw(scroll_line);
    
    sf::RectangleShape scroll_button(sf::Vector2f(button_dx, button_dy));
    scroll_button.setFillColor(SCROLL_BUTTON_COLOR);
    scroll_button.setPosition(button_x, button_y);
    window.draw(scroll_button);
}


//class ExperimentView

ExperimentView::ExperimentView() : view_x(VIEW_DEFAULT_X), view_y(VIEW_DEFAULT_Y), scale(VIEW_DEFAULT_SCALE) {}

void ExperimentView::clear()
{
    view_x = VIEW_DEFAULT_X;
    view_y = VIEW_DEFAULT_Y;
    scale = VIEW_DEFAULT_SCALE;
}

void ExperimentView::move(double delta_x, double delta_y)
{
    view_x += delta_x;
    view_y += delta_y;
}
    
void ExperimentView::get_screen_position(double local_x, double local_y, double &screen_x, double &screen_y) const
{
    screen_x = local_x / METERS_PER_PIXEL * scale + view_x;
    screen_y = local_y / METERS_PER_PIXEL * scale + view_y;
}

void ExperimentView::get_local_position(double screen_x, double screen_y, double &local_x, double &local_y) const
{
    local_x = (screen_x - view_x) / scale * METERS_PER_PIXEL;
    local_y = (screen_y - view_y) / scale * METERS_PER_PIXEL;
}

void ExperimentView::zoom_in(double screen_x, double screen_y)
{
    double local_x, local_y;
    this->get_local_position(screen_x, screen_y, local_x, local_y);
    
    scale /= VIEW_SCALE_STEP_MULTIPLIER;
    if (scale < VIEW_SCALE_MIN) {
        scale = VIEW_SCALE_MIN;
    }
    this->focus_on_target(local_x, local_y);
    this->move(screen_x - VIEW_DEFAULT_X, screen_y - VIEW_DEFAULT_Y);
}

void ExperimentView::zoom_out(double screen_x, double screen_y)
{
    double local_x, local_y;
    this->get_local_position(screen_x, screen_y, local_x, local_y);
    
    scale *= VIEW_SCALE_STEP_MULTIPLIER;
    if (scale > VIEW_SCALE_MAX) {
        scale = VIEW_SCALE_MAX;
    }
    this->focus_on_target(local_x, local_y);
    this->move(screen_x - VIEW_DEFAULT_X, screen_y - VIEW_DEFAULT_Y);
}

void ExperimentView::focus_on_target(double target_local_x, double target_local_y)
{
    double target_screen_x, target_screen_y;
    this->get_screen_position(target_local_x, target_local_y, target_screen_x, target_screen_y);
    view_x += VIEW_DEFAULT_X - target_screen_x;
    view_y += VIEW_DEFAULT_Y - target_screen_y;
}


//FUNCTIONS

void draw_object(sf::RenderWindow &window, const CelestialBody *object, const ExperimentView &view,
    const sf::Font &font, bool show_names)
{
    double radius_to_draw = std::max(MIN_DRAWING_RADIUS, object->radius / METERS_PER_PIXEL * view.scale);
    double screen_x, screen_y;
    view.get_screen_position(object->x, object->y, screen_x, screen_y);
    
    sf::Color color;
    switch (object->type) {
        case objectStar:
            color = STAR_COLOR;
            break;
        case objectPlanet:
            color = PLANET_COLOR;
            break;
        case objectComet:
            color = COMET_COLOR;
            break;
        case objectSatellite:
            color = SATELLITE_COLOR;
            break;
        default:
            color = OTHER_OBJECT_COLOR;
            break;
    }
    
    sf::CircleShape circle(radius_to_draw);
    circle.setPosition(screen_x - radius_to_draw, screen_y - radius_to_draw);
    circle.setFillColor(color);
    window.draw(circle);
    
    if (show_names) {
        sf::Text text;
        text.setFont(font);
        text.setFillColor(sf::Color::White);
        text.setCharacterSize(DRAW_OBJECT_FONT_SIZE);
        text.setString(object->name);
        text.setPosition(screen_x, screen_y + radius_to_draw);
        window.draw(text);
    }
}

void show_experiment(sf::RenderWindow &window, const Experiment &experiment, const ExperimentView &view,
    const sf::Font &font, int show_names_mode)
{
    for (int i = 0; i < experiment.size(); ++i) {
        const CelestialBody *object = experiment.objects(i);
        bool show_names = show_names_mode != showNoNames &&
            (show_names_mode == showAllNames || object->type == objectStar || object->type == objectPlanet);
        draw_object(window, object, view, font, show_names);
    }
}

void draw_trace(sf::RenderWindow &window, const CelestialBody *object, const ExperimentView &view)
{
    sf::VertexArray lines(sf::LineStrip, object->points.size());
    for (int k = 0; k < object->points.size(); ++k) {
        double screen_x, screen_y;
        view.get_screen_position(object->points[k].first, object->points[k].second, screen_x, screen_y);
        lines[k].position = sf::Vector2f(screen_x, screen_y);
    }
    window.draw(lines);
}

void draw_object_info(sf::RenderWindow &window, const Experiment &experiment, const CelestialBody *object,
        const ExperimentView &view, const sf::Font &font, ScrollField &field)
{
    std::vector<std::string> detailed_info;
    detailed_info.push_back("Name: " + short_name(object->name, 30));
    
    std::string nametype;
    switch (object->type) {
        case objectStar:
            nametype = "star";
            break;
        case objectPlanet:
            nametype = "planet";
            break;
        case objectComet:
            nametype = "comet";
            break;
        case objectSatellite:
            nametype = "satellite";
            break;
        default:
            nametype = "unknown";
            break;
    }
    detailed_info.push_back("Type: " + nametype);
    
    double mass = 0;
    std::vector<std::pair<double, int>> forces;
    for (int i = 0; i < experiment.size(); ++i) {
        mass += experiment.objects(i)->weight;
        if (dist(experiment.objects(i), object) > EPS) {
            forces.push_back(std::pair<double, int>(gravity_force(experiment.objects(i), object), i));
        }
    }
    std::sort(forces.begin(), forces.end());
    std::reverse(forces.begin(), forces.end());
    
    std::ostringstream stream;
    stream << "Weight: " << " " << object->weight;
    stream << std::fixed << " (" << (object->weight / mass) * 100 << "%)";
    detailed_info.push_back(stream.str());
    stream.str("");
    
    stream << std::scientific;
    stream << "speed: " << vector_length(object->speed_x, object->speed_y) << " m/s";
    detailed_info.push_back(stream.str());
    stream.str("");
    
    int top_size = forces.size() > 3 ? 3 : forces.size();
    detailed_info.push_back("");
    stream << "Top " << top_size << " other objects that affect this:";
    detailed_info.push_back(stream.str());
    stream.str("");
    for (int i = 0; i < top_size; ++i) {
        stream << experiment.objects(forces[i].second)->name;
        stream << ": " << forces[i].first << " H";
        detailed_info.push_back(stream.str());
        stream.str("");
    }
    
    field.resize(detailed_info.size());
    field.draw(window, detailed_info, font);
}

void draw_experiment_info(sf::RenderWindow &window, const Experiment &experiment, const sf::Font &font,
    ScrollField &field, std::vector<const CelestialBody *> &object_ptrs)
{
    std::vector<std::string> objects_info;
    objects_info.push_back("List of all objects in this planet system.");
    objects_info.push_back("Click on object name to get more information about it.");
    objects_info.push_back("");
    
    object_ptrs.clear();
    object_ptrs.push_back(nullptr);
    object_ptrs.push_back(nullptr);
    object_ptrs.push_back(nullptr);
    
    for (int i = 0; i < experiment.size(); ++i) {
        std::ostringstream stream;
        const CelestialBody *object = experiment.objects(i);
        
        std::string nametype;
        switch (object->type) {
            case objectStar:
                nametype = "star";
                break;
            case objectPlanet:
                nametype = "planet";
                break;
            case objectComet:
                nametype = "comet";
                break;
            case objectSatellite:
                nametype = "satellite";
                break;
            default:
                nametype = "unknown";
                break;
        }
        stream << nametype << " " << short_name(object->name, 30) << ", ";
        stream << object->weight << " kg, ";
        stream << "speed: " << vector_length(object->speed_x, object->speed_y) << " m/s";
        objects_info.push_back(stream.str());
        object_ptrs.push_back(object);
    }
    field.resize(objects_info.size());
    field.draw(window, objects_info, font);
}

std::vector<Button> define_buttons(GraphStorage &pictures)
{
    std::vector<Button> buttons(BUTTON_MAX);
    buttons[quitGame]  = Button(40, 600, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT, quitGame, mainMenu);
    buttons[toInstr]   = Button(40, 500, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT, toInstr, mainMenu);
    buttons[startGame] = Button(40, 400, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT, startGame, mainMenu);
    buttons[executeAll]     = Button( 10, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeAll, gameRoom);
    buttons[executePause]   = Button(110, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executePause, gameRoom);
    buttons[executeStep]    = Button(210, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeStep, gameRoom);
    buttons[executeSpeed1]   = Button(310, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeSpeed1, gameRoom);
    buttons[executeSpeed4]   = Button(410, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeSpeed4, gameRoom);
    buttons[executeSpeed16]  = Button(510, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeSpeed16, gameRoom);
    buttons[executeSpeed64]  = Button(610, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeSpeed64, gameRoom);
    buttons[executeSpeed365] = Button(710, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeSpeed365, gameRoom);
    buttons[showInfo] = Button(910, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, showInfo, gameRoom);
    buttons[addComet] = Button(810, 20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, addComet, gameRoom);
    buttons[switchShowingNames] = Button(10, 120, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, switchShowingNames, gameRoom);
    buttons[switchShowingOrbits] = Button(10, 220, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, switchShowingOrbits, gameRoom);
    buttons[toMainMenu]  = Button(10, 620, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, toMainMenu, gameRoom);
    buttons[typingOK]     = Button(860, 320, 120, 80, typingOK, inputRoom);
    buttons[typingClear]  = Button(560, 320, 80, 80, typingClear, inputRoom);
    buttons[typingCancel] = Button(300, 320, 200, 80, typingCancel, inputRoom);
    buttons[backInstr]    = Button(40, 600, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT, backInstr, instrRoom);
    
    for (int i = 0; i < BUTTON_MAX; ++i) {
        pictures.buttons[i].setTextureRect(sf::IntRect(0, 0, buttons[i].dx, buttons[i].dy));
        pictures.buttons[i].setPosition(buttons[i].x, buttons[i].y);
    }
    
    return buttons;
}