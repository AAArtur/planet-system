#include <fstream>
#include <vector>
#include <cmath>
#include <memory>

#ifndef _physics_h
#define _physics_h


enum ObjectType {
    objectUnknown,
    objectPlanet,
    objectStar,
    objectComet,
    objectSatellite
};


struct CelestialBody {
    std::string name;
    double weight;
    double radius;
    double x, y;
    double speed_x, speed_y;
    int type;
    std::vector<std::pair<double, double>> points;
    
    CelestialBody(std::string name, double w, double r, double x, double y, double sx, double sy);
    virtual ~CelestialBody();
};


struct Planet: public CelestialBody {
    Planet(std::string name, double w, double r, double x, double y, double sx, double sy);
    virtual ~Planet();
};


struct Star: public CelestialBody {
    Star(std::string name, double w, double r, double x, double y, double sx, double sy);
    virtual ~Star();
};


struct Comet: public CelestialBody {
    Comet(std::string name, double w, double r, double x, double y, double sx, double sy);
    virtual ~Comet();
};


struct Satellite: public CelestialBody {
    Satellite(std::string name, double w, double r, double x, double y, double sx, double sy);
    virtual ~Satellite();
};


class ObjectSystem {
private:
    std::vector<std::shared_ptr<CelestialBody>> alive_objects;
    std::vector<std::shared_ptr<CelestialBody>> dead_objects;
    
public:
    ObjectSystem();
    
    const CelestialBody *objects(int i) const;
    int size() const;
    void add_object(CelestialBody *object);
    void next_day(std::vector<std::string> &log, int day_number);
};


class Experiment {
private:
    int days;
    int days_per_second;
    ObjectSystem object_system;
    
public:
    Experiment();
    
    void set_days_per_second(int d);
    int get_days_per_second() const;
    int get_expired_days() const;
    const CelestialBody *objects(int i) const;
    int size() const;
    
    void clear();
    void add_object(CelestialBody *object);
    void next_day(std::vector<std::string> &log);
    bool load_from_file(std::string filename, std::vector<std::string> &log);
};


//FUNCTIONS

std::string short_name(std::string name, int k);
double vector_length(double x, double y);
double dist(const CelestialBody *b1, const CelestialBody *b2);

double gravity_force_part(const CelestialBody *b1, const CelestialBody *b2);   // F' = G / r^2
double gravity_force(const CelestialBody *b1, const CelestialBody *b2);        // F  = G * m1 * m2 / r^2
void calculate_acceleration(const CelestialBody *b1, const CelestialBody *b2,  // a = F / m
    double &ax1, double &ay1, double &ax2, double &ay2);
void calculate_vx_vy(double dx, double dy, double v, double &vx, double &vy);  // direction, |V| -> Vx, Vy


//CONSTANTS

const int DAYS_PER_SECOND_DEFAULT = 1;

const int TICKS_PER_DAY = 1000;  //more ticks per day - more precize calculation and more stress to computer
const int SECONDS_IN_DAY = 86400;

const double G = 6.67e-11;
const double EPS = 1e-10;


#endif