#include <SFML/Graphics.hpp>
#include <vector>
#include <memory>
#include <fstream>
#include <sstream>

#include "physics.h"

#ifndef _graph_h
#define _graph_h

//DATA STRUCTURES

enum Room {
    NO_ROOM = -1,
    mainMenu = 0,
    gameRoom,
    instrRoom,
    inputRoom,

    //number of rooms
    ROOM_MAX,
};

enum ButtonName {
    NO_BUTTON = -1,
    startGame = 0,
    toMainMenu,
    quitGame,
    toInstr,
    backInstr,

    executeStep,
    executeAll,
    executePause,
    executeSpeed1,
    executeSpeed4,
    executeSpeed16,
    executeSpeed64,
    executeSpeed365,
    showInfo,
    addComet,
    switchShowingNames,
    switchShowingOrbits,

    typingOK,
    typingClear,
    typingCancel,

    //number of buttons
    BUTTON_MAX
};

enum CreatingCometStep {
    noCreatingComet = 0,
    choosingCometStartPos,
    choosingCometDestinationPos,
    enteringCometWeight,
    enteringCometSpeed,
    enteringCometRadius,
    enteringCometName
};

enum ShowingNamesMode {
    showAllNames = 0,
    showPlanetStarsNames,
    showNoNames
};

enum ShowingTraceMode {
    showAllTrace = 0,
    showOneTrace,
    showNoTrace
};


class GraphStorage {
private:
    std::vector<std::shared_ptr<sf::Texture>> back_textures;
    std::vector<std::shared_ptr<sf::Texture>> button_textures;
    std::vector<std::shared_ptr<sf::Texture>> other_textures;

public:
    std::vector<sf::Sprite> backs;
    std::vector<sf::Sprite> buttons;

    GraphStorage();

    bool load_backs(std::string filename);
    bool load_buttons(std::string filename);
};


struct Button {
    int x, y;
    int dx, dy;
    int name;
    int room;

    Button();
    Button(int fx, int fy, int fdx, int fdy, int fname, int froom);

    bool is_touched(int mouse_x, int mouse_y) const;
};


class TextField {
private:
    std::string content;
    int pos;
    
    int font_width;
    int text_screen_x() const;
    int text_screen_y() const;
    int get_touched_position(int mouse_x) const;

public:
    int x, y;
    int dx, dy;
    int font_size;
    int max_size;

    TextField(int x, int y, int dx, int dy, int font_size, int max_size);

    std::string get_string() const;
    void clear();
    void put_char(char ch);
    void right();
    void left();
    void end();
    void home();
    void del();
    void backspace();
    
    bool is_touched(int mouse_x, int mouse_y) const;
    void set_string(std::string str);
    void set_position(int mouse_x);
    void draw(sf::RenderWindow &window, sf::Font &font, bool draw_cursor) const;
};


class ScrollField {
private:
    int x, y;
    int dx, dy;
    int size;
    float scrolling, max_scrolling;
    int button_x, button_y, button_dx, button_dy;
    int pixels_per_scroll;
    int text_offset_y;
    int font_size, font_gap;
    int font_width;
    float get_max_scrolling() const;
    void update_button();
    int top_visible_line() const;
    int bottom_visible_line() const;
    int text_screen_x() const;
    int text_screen_y(int i) const;

public:
    ScrollField(int x, int y, int dx, int dy, int font_size, int font_gap);
    void scroll_up();
    void scroll_down();
    void scroll_with_mouse(int mouse_y);
    void scroll_to_top();
    void scroll_to_bottom();
    void resize(int new_size);
    
    bool is_touched(int mouse_x, int mouse_y) const;
    bool is_button_touched(int mouse_x, int mouse_y);
    
    int get_touched_position(int mouse_y) const;
    
    void draw(sf::RenderWindow &window, const std::vector<std::string> &strs, const sf::Font &font);
};


class ExperimentView {
public:
    double view_x, view_y; // where to draw (0, 0)-coord
    double scale;
    
    ExperimentView();
    void clear();
    
    void move(double delta_x, double delta_y);
    void get_screen_position(double local_x, double local_y, double &screen_x, double &screen_y) const;
    void get_local_position(double screen_x, double screen_y, double &local_x, double &local_y) const;
    void zoom_in(double screen_x, double screen_y);
    void zoom_out(double screen_x, double screen_y);
    void focus_on_target(double target_local_x, double target_local_y);
};


//FUNCTIONS

std::vector<Button> define_buttons(GraphStorage &textures);

void show_experiment(sf::RenderWindow &window, const Experiment &exp, const ExperimentView &view, const sf::Font &font, int show_names_mode);
void draw_object(sf::RenderWindow &window, const CelestialBody *object, const ExperimentView &view, const sf::Font &font, bool show_names);

void draw_trace(sf::RenderWindow &window, const CelestialBody *object, const ExperimentView &view);
void draw_object_info(sf::RenderWindow &window, const Experiment &experiment, const CelestialBody *object,
    const ExperimentView &view, const sf::Font &font, ScrollField &field);
void draw_experiment_info(sf::RenderWindow &window, const Experiment &experiment, const sf::Font &font,
    ScrollField &field, std::vector<const CelestialBody *> &object_ptrs);

//CONSTANTS

const std::string FONT_FILENAME = "RobotoMono-Regular.ttf";
const float FONT_WIDTH_COEF = 0.6;
const std::string BACKS_FILENAME = "pictures/backs/";
const std::string BUTTONS_FILENAME = "pictures/buttons/";

const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 720;

const int DRAW_OBJECT_FONT_SIZE = 20;

const int MOUSE_MAX_DIST_FOR_IDLE = 3;

const int INFO_FONT_SIZE = 20;
const int INFO_FONT_GAP = 1;
const int INFO_FIELD_X = 20;
const int INFO_FIELD_Y = 20;
const int INFO_FIELD_DX = 900;
const int INFO_FIELD_DY = 680;

const int DETAILED_INFO_FONT_SIZE = 20;
const int DETAILED_INFO_FONT_GAP = 1;
const int DETAILED_INFO_FIELD_X = 780;
const int DETAILED_INFO_FIELD_Y = 170;
const int DETAILED_INFO_FIELD_DX = 500;
const int DETAILED_INFO_FIELD_DY = 270;

const int LOG_FONT_SIZE = 18;
const int LOG_FONT_GAP = 1;
const int LOG_FIELD_X = 780;
const int LOG_FIELD_Y = 490;
const int LOG_FIELD_DX = 500;
const int LOG_FIELD_DY = 210;

const int USER_FILEPATH_FONT_SIZE = 32;
const int FILEPATH_MAX_SIZE = 32;
const int USER_INPUT_X = 200;
const int USER_INPUT_Y = 220;
const int USER_INPUT_DX = 880;
const int USER_INPUT_DY = 50;

const int COMET_INPUT_FONT_SIZE = 24;
const int COMET_INPUT_MAX_SIZE = 20;
const int COMET_INPUT_X = 100;
const int COMET_INPUT_Y = 140;
const int COMET_INPUT_DX = 460;
const int COMET_INPUT_DY = 40;

const int SCROLL_BUTTON_WIDTH = 20;
const int BIG_BUTTON_WIDTH = 300;
const int BIG_BUTTON_HEIGHT = 80;
const int MINI_BUTTON_WIDTH = 80;
const int MINI_BUTTON_HEIGHT = 80;

const int CURSOR_TIME_VISIBLE_MSEC = 600;
const int CURSOR_TIME_INVISIBLE_MSEC = 100;

const sf::Color APP_TEXT_COLOR = sf::Color(0, 0, 0, 255);
const sf::Color TEXT_BACKGROUND_COLOR = sf::Color(255, 255, 255, 200);
const sf::Color SCROLL_BORDER_COLOR = sf::Color(64, 64, 64, 255);
const sf::Color SCROLL_LINE_COLOR = sf::Color(160, 160, 160, 255);
const sf::Color SCROLL_BUTTON_COLOR = sf::Color(128, 128, 128, 255);

const sf::Color PLANET_COLOR = sf::Color(0, 255, 0, 255);
const sf::Color STAR_COLOR = sf::Color(255, 255, 0, 255);
const sf::Color COMET_COLOR = sf::Color(255, 0, 0, 255);
const sf::Color SATELLITE_COLOR = sf::Color(137, 55, 0, 255);
const sf::Color OTHER_OBJECT_COLOR = sf::Color(170, 170, 170, 255);

const int METERS_PER_PIXEL = 500000;
const double MIN_DRAWING_RADIUS = 3;
const int TRACE_MAX_SIZE = 800;

const int VIEW_DEFAULT_X = WINDOW_WIDTH / 2;
const int VIEW_DEFAULT_Y = WINDOW_HEIGHT / 2;
const double VIEW_DEFAULT_SCALE = 0.4;
const double VIEW_SCALE_STEP_MULTIPLIER = 1.5;
const double VIEW_SCALE_MIN = 0.00001;
const double VIEW_SCALE_MAX = 1.0;

#endif