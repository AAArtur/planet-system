#!/bin/bash

g++ source/main.cpp source/physics.cpp source/graph.cpp -o work/prog -I ./SFML/SFML-2.5.1/include -L ./SFML/SFML-2.5.1/lib -lsfml-graphics -lsfml-window -lsfml-system 2> compilation_log.txt
head compilation_log.txt -n 10

